//
//  APIModels.swift
//  WeatherApp
//
//  Created by Aidan Walker on 06/06/2023.
//

import Foundation

struct APIResponse: Codable {
    let lat: Float
    let lon: Float
    let current: CurrentModel
    let hourly: [HourlyModel]
    let daily: [DayModel]
}

// Current Model
struct CurrentModel: Codable {
    let temp: Double
    let weather: [InfoModel]
}

// Hourly Model
struct HourlyModel: Codable {
    let dt: Float // day time
    let temp: Double
    let weather: [InfoModel]
}

// Info
struct InfoModel: Codable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}

// Daily Model
struct DayModel: Codable {
    let dt: Float // day time
    let temp: TempModel
}

struct TempModel: Codable {
    let min: Double
    let max: Double
}


// **** TUTORIAL DATA ****
//{
//lat: 55.9533,
//lon: -3.1883,
//timezone: "Europe/London",
//timezone_offset: 3600,
//current: {
//dt: 1686055154,
//sunrise: 1686022256,
//sunset: 1686084725,
//temp: 57,
//feels_like: 55.65,
//pressure: 1027,
//humidity: 69,
//dew_point: 46.94,
//uvi: 5.88,
//clouds: 40,
//visibility: 10000,
//wind_speed: 10.36,
//wind_deg: 40,
//weather: [
//{
//id: 802,
//main: "Clouds",
//description: "scattered clouds",
//icon: "03d"
//}
//]
//},
//hourly: [
//{
//dt: 1686052800,
//temp: 56.95,
//feels_like: 55.54,
//pressure: 1027,
//humidity: 68,
//dew_point: 46.51,
//uvi: 5.76,
//clouds: 42,
//visibility: 10000,
//wind_speed: 9.37,
//wind_deg: 61,
//wind_gust: 8.5,
//weather: [
//{
//id: 802,
//main: "Clouds",
//description: "scattered clouds",
//icon: "03d"
//}
//],
//pop: 0
//},
//{
//dt: 1686056400,
//temp: 57,
//feels_like: 55.65,
//pressure: 1027,
//humidity: 69,
//dew_point: 46.94,
//uvi: 5.88,
//clouds: 40,
//visibility: 10000,
//wind_speed: 9.48,
//wind_deg: 60,
//wind_gust: 8.52,
//weather: [
//{
//id: 802,
//main: "Clouds",
//description: "scattered clouds",
//icon: "03d"
//}
//],
//pop: 0
//},
//{
//dt: 1686060000,
//temp: 57,
//feels_like: 55.6,
//pressure: 1027,
//humidity: 68,
//dew_point: 46.56,
//uvi: 5.13,
//clouds: 34,
//visibility: 10000,
//wind_speed: 9.42,
//wind_deg: 59,
//wind_gust: 8.63,
//weather: [
//{
//id: 802,
//main: "Clouds",
//description: "scattered clouds",
//icon: "03d"
//}
//],
//pop: 0
//},
//{
//dt: 1686063600,
//temp: 56.82,
//feels_like: 55.4,
//pressure: 1027,
//humidity: 68,
//dew_point: 46.38,
//uvi: 3.99,
//clouds: 28,
//visibility: 10000,
//wind_speed: 8.97,
//wind_deg: 58,
//wind_gust: 8.23,
//weather: [
//{
//id: 802,
//main: "Clouds",
//description: "scattered clouds",
//icon: "03d"
//}
//],
//pop: 0
//},
//{
//dt: 1686067200,
//temp: 56.44,
//feels_like: 55.04,
//pressure: 1026,
//humidity: 69,
//dew_point: 46.42,
//uvi: 2.76,
//clouds: 21,
//visibility: 10000,
//wind_speed: 8.7,
//wind_deg: 60,
//wind_gust: 8.34,
//weather: [
//{
//id: 801,
//main: "Clouds",
//description: "few clouds",
//icon: "02d"
//}
//],
//pop: 0
//},
//{
//dt: 1686070800,
//temp: 55.58,
//feels_like: 54.18,
//pressure: 1026,
//humidity: 71,
//dew_point: 46.35,
//uvi: 1.63,
//clouds: 15,
//visibility: 10000,
//wind_speed: 8.3,
//wind_deg: 60,
//wind_gust: 8.63,
//weather: [
//{
//id: 801,
//main: "Clouds",
//description: "few clouds",
//icon: "02d"
//}
//],
//pop: 0
//},
//{
//dt: 1686074400,
//temp: 54.23,
//feels_like: 52.79,
//pressure: 1026,
//humidity: 73,
//dew_point: 45.46,
//uvi: 0.81,
//clouds: 8,
//visibility: 10000,
//wind_speed: 7.78,
//wind_deg: 62,
//wind_gust: 8.88,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686078000,
//temp: 52.79,
//feels_like: 51.39,
//pressure: 1026,
//humidity: 77,
//dew_point: 45.52,
//uvi: 0.33,
//clouds: 4,
//visibility: 10000,
//wind_speed: 6.91,
//wind_deg: 61,
//wind_gust: 9.04,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686081600,
//temp: 50.99,
//feels_like: 49.64,
//pressure: 1027,
//humidity: 82,
//dew_point: 45.19,
//uvi: 0.09,
//clouds: 2,
//visibility: 10000,
//wind_speed: 6.33,
//wind_deg: 61,
//wind_gust: 9.48,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686085200,
//temp: 48.9,
//feels_like: 46.54,
//pressure: 1027,
//humidity: 86,
//dew_point: 44.28,
//uvi: 0,
//clouds: 2,
//visibility: 10000,
//wind_speed: 5.66,
//wind_deg: 67,
//wind_gust: 8.1,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01n"
//}
//],
//pop: 0
//},
//{
//dt: 1686088800,
//temp: 48,
//feels_like: 46.11,
//pressure: 1027,
//humidity: 87,
//dew_point: 43.9,
//uvi: 0,
//clouds: 1,
//visibility: 10000,
//wind_speed: 4.63,
//wind_deg: 72,
//wind_gust: 6.62,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01n"
//}
//],
//pop: 0
//},
//{
//dt: 1686092400,
//temp: 47.34,
//feels_like: 46.06,
//pressure: 1027,
//humidity: 88,
//dew_point: 43.63,
//uvi: 0,
//clouds: 1,
//visibility: 10000,
//wind_speed: 3.67,
//wind_deg: 74,
//wind_gust: 5.7,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01n"
//}
//],
//pop: 0
//},
//{
//dt: 1686096000,
//temp: 46.71,
//feels_like: 46.71,
//pressure: 1027,
//humidity: 89,
//dew_point: 43.2,
//uvi: 0,
//clouds: 1,
//visibility: 10000,
//wind_speed: 2.77,
//wind_deg: 73,
//wind_gust: 4.18,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01n"
//}
//],
//pop: 0
//},
//{
//dt: 1686099600,
//temp: 45.9,
//feels_like: 45.9,
//pressure: 1027,
//humidity: 90,
//dew_point: 42.66,
//uvi: 0,
//clouds: 0,
//visibility: 10000,
//wind_speed: 1.9,
//wind_deg: 69,
//wind_gust: 3.36,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01n"
//}
//],
//pop: 0
//},
//{
//dt: 1686103200,
//temp: 45.37,
//feels_like: 45.37,
//pressure: 1026,
//humidity: 90,
//dew_point: 42.21,
//uvi: 0,
//clouds: 0,
//visibility: 10000,
//wind_speed: 1.43,
//wind_deg: 57,
//wind_gust: 2.71,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01n"
//}
//],
//pop: 0
//},
//{
//dt: 1686106800,
//temp: 44.87,
//feels_like: 44.87,
//pressure: 1026,
//humidity: 91,
//dew_point: 41.77,
//uvi: 0,
//clouds: 0,
//visibility: 10000,
//wind_speed: 1.36,
//wind_deg: 41,
//wind_gust: 2.37,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01n"
//}
//],
//pop: 0
//},
//{
//dt: 1686110400,
//temp: 44.64,
//feels_like: 44.64,
//pressure: 1026,
//humidity: 91,
//dew_point: 41.72,
//uvi: 0,
//clouds: 0,
//visibility: 10000,
//wind_speed: 1.7,
//wind_deg: 45,
//wind_gust: 2.3,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686114000,
//temp: 46.54,
//feels_like: 46.54,
//pressure: 1026,
//humidity: 88,
//dew_point: 42.71,
//uvi: 0.2,
//clouds: 0,
//visibility: 10000,
//wind_speed: 1.61,
//wind_deg: 52,
//wind_gust: 2.48,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686117600,
//temp: 49.08,
//feels_like: 49.08,
//pressure: 1026,
//humidity: 81,
//dew_point: 43.27,
//uvi: 0.55,
//clouds: 0,
//visibility: 10000,
//wind_speed: 2.28,
//wind_deg: 53,
//wind_gust: 3.15,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686121200,
//temp: 51.62,
//feels_like: 50,
//pressure: 1026,
//humidity: 75,
//dew_point: 43.5,
//uvi: 1.19,
//clouds: 0,
//visibility: 10000,
//wind_speed: 3.33,
//wind_deg: 61,
//wind_gust: 4.27,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686124800,
//temp: 53.91,
//feels_like: 52.23,
//pressure: 1026,
//humidity: 69,
//dew_point: 43.41,
//uvi: 2.19,
//clouds: 0,
//visibility: 10000,
//wind_speed: 4.25,
//wind_deg: 63,
//wind_gust: 4.97,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686128400,
//temp: 55.6,
//feels_like: 53.91,
//pressure: 1026,
//humidity: 65,
//dew_point: 43.65,
//uvi: 3.41,
//clouds: 0,
//visibility: 10000,
//wind_speed: 5.66,
//wind_deg: 65,
//wind_gust: 6.29,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686132000,
//temp: 57.31,
//feels_like: 55.74,
//pressure: 1026,
//humidity: 64,
//dew_point: 44.47,
//uvi: 4.62,
//clouds: 0,
//visibility: 10000,
//wind_speed: 6.35,
//wind_deg: 63,
//wind_gust: 6.91,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686135600,
//temp: 58.75,
//feels_like: 57.29,
//pressure: 1026,
//humidity: 63,
//dew_point: 45.54,
//uvi: 5.58,
//clouds: 0,
//visibility: 10000,
//wind_speed: 7.2,
//wind_deg: 58,
//wind_gust: 7.61,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686139200,
//temp: 59.85,
//feels_like: 58.53,
//pressure: 1026,
//humidity: 64,
//dew_point: 46.53,
//uvi: 6.03,
//clouds: 0,
//visibility: 10000,
//wind_speed: 8.41,
//wind_deg: 56,
//wind_gust: 8.57,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686142800,
//temp: 60.13,
//feels_like: 58.91,
//pressure: 1025,
//humidity: 65,
//dew_point: 47.55,
//uvi: 5.69,
//clouds: 6,
//visibility: 10000,
//wind_speed: 9.24,
//wind_deg: 56,
//wind_gust: 9.15,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686146400,
//temp: 59.9,
//feels_like: 58.69,
//pressure: 1025,
//humidity: 66,
//dew_point: 47.98,
//uvi: 4.97,
//clouds: 7,
//visibility: 10000,
//wind_speed: 10.11,
//wind_deg: 54,
//wind_gust: 9.84,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686150000,
//temp: 59.74,
//feels_like: 58.57,
//pressure: 1025,
//humidity: 67,
//dew_point: 48.25,
//uvi: 3.87,
//clouds: 6,
//visibility: 10000,
//wind_speed: 10.78,
//wind_deg: 55,
//wind_gust: 11.54,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686153600,
//temp: 59.61,
//feels_like: 58.48,
//pressure: 1025,
//humidity: 68,
//dew_point: 48.38,
//uvi: 2.7,
//clouds: 4,
//visibility: 10000,
//wind_speed: 11.1,
//wind_deg: 58,
//wind_gust: 12.44,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686157200,
//temp: 59.22,
//feels_like: 58.08,
//pressure: 1025,
//humidity: 69,
//dew_point: 48.52,
//uvi: 1.6,
//clouds: 4,
//visibility: 10000,
//wind_speed: 10.54,
//wind_deg: 62,
//wind_gust: 13.11,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686160800,
//temp: 58.19,
//feels_like: 57.09,
//pressure: 1025,
//humidity: 72,
//dew_point: 48.7,
//uvi: 0.8,
//clouds: 3,
//visibility: 10000,
//wind_speed: 10.51,
//wind_deg: 63,
//wind_gust: 13.62,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686164400,
//temp: 56.12,
//feels_like: 55.06,
//pressure: 1025,
//humidity: 77,
//dew_point: 48.74,
//uvi: 0.32,
//clouds: 0,
//visibility: 10000,
//wind_speed: 8.7,
//wind_deg: 62,
//wind_gust: 13.15,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686168000,
//temp: 53.31,
//feels_like: 52.29,
//pressure: 1025,
//humidity: 84,
//dew_point: 47.98,
//uvi: 0.09,
//clouds: 3,
//visibility: 10000,
//wind_speed: 7.05,
//wind_deg: 57,
//wind_gust: 10.27,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//pop: 0
//},
//{
//dt: 1686171600,
//temp: 50.65,
//feels_like: 49.55,
//pressure: 1026,
//humidity: 88,
//dew_point: 46.9,
//uvi: 0,
//clouds: 4,
//visibility: 10000,
//wind_speed: 6.15,
//wind_deg: 61,
//wind_gust: 8.72,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01n"
//}
//],
//pop: 0
//},
//{
//dt: 1686175200,
//temp: 49.55,
//feels_like: 47.41,
//pressure: 1026,
//humidity: 89,
//dew_point: 46.13,
//uvi: 0,
//clouds: 7,
//visibility: 10000,
//wind_speed: 5.48,
//wind_deg: 68,
//wind_gust: 8.28,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01n"
//}
//],
//pop: 0
//},
//{
//dt: 1686178800,
//temp: 48.49,
//feels_like: 47.03,
//pressure: 1026,
//humidity: 90,
//dew_point: 45.16,
//uvi: 0,
//clouds: 14,
//visibility: 10000,
//wind_speed: 4.12,
//wind_deg: 70,
//wind_gust: 6.62,
//weather: [
//{
//id: 801,
//main: "Clouds",
//description: "few clouds",
//icon: "02n"
//}
//],
//pop: 0
//},
//{
//dt: 1686182400,
//temp: 47.75,
//feels_like: 46.69,
//pressure: 1026,
//humidity: 90,
//dew_point: 44.28,
//uvi: 0,
//clouds: 23,
//visibility: 10000,
//wind_speed: 3.49,
//wind_deg: 73,
//wind_gust: 5.46,
//weather: [
//{
//id: 801,
//main: "Clouds",
//description: "few clouds",
//icon: "02n"
//}
//],
//pop: 0
//},
//{
//dt: 1686186000,
//temp: 47.16,
//feels_like: 46.17,
//pressure: 1025,
//humidity: 89,
//dew_point: 43.74,
//uvi: 0,
//clouds: 99,
//visibility: 10000,
//wind_speed: 3.31,
//wind_deg: 67,
//wind_gust: 4.83,
//weather: [
//{
//id: 804,
//main: "Clouds",
//description: "overcast clouds",
//icon: "04n"
//}
//],
//pop: 0
//},
//{
//dt: 1686189600,
//temp: 46.74,
//feels_like: 45.43,
//pressure: 1025,
//humidity: 89,
//dew_point: 43.32,
//uvi: 0,
//clouds: 99,
//visibility: 10000,
//wind_speed: 3.6,
//wind_deg: 65,
//wind_gust: 5.12,
//weather: [
//{
//id: 804,
//main: "Clouds",
//description: "overcast clouds",
//icon: "04n"
//}
//],
//pop: 0
//},
//{
//dt: 1686193200,
//temp: 46.17,
//feels_like: 45.34,
//pressure: 1025,
//humidity: 89,
//dew_point: 42.8,
//uvi: 0,
//clouds: 100,
//visibility: 10000,
//wind_speed: 3,
//wind_deg: 65,
//wind_gust: 4.56,
//weather: [
//{
//id: 804,
//main: "Clouds",
//description: "overcast clouds",
//icon: "04n"
//}
//],
//pop: 0
//},
//{
//dt: 1686196800,
//temp: 46.09,
//feels_like: 46.09,
//pressure: 1025,
//humidity: 89,
//dew_point: 42.67,
//uvi: 0,
//clouds: 100,
//visibility: 10000,
//wind_speed: 2.77,
//wind_deg: 59,
//wind_gust: 4.18,
//weather: [
//{
//id: 804,
//main: "Clouds",
//description: "overcast clouds",
//icon: "04d"
//}
//],
//pop: 0
//},
//{
//dt: 1686200400,
//temp: 47.75,
//feels_like: 47.75,
//pressure: 1025,
//humidity: 87,
//dew_point: 43.56,
//uvi: 0.2,
//clouds: 100,
//visibility: 10000,
//wind_speed: 2.86,
//wind_deg: 56,
//wind_gust: 4.59,
//weather: [
//{
//id: 804,
//main: "Clouds",
//description: "overcast clouds",
//icon: "04d"
//}
//],
//pop: 0
//},
//{
//dt: 1686204000,
//temp: 50.36,
//feels_like: 48.9,
//pressure: 1025,
//humidity: 81,
//dew_point: 44.13,
//uvi: 0.54,
//clouds: 95,
//visibility: 10000,
//wind_speed: 3.6,
//wind_deg: 58,
//wind_gust: 5.73,
//weather: [
//{
//id: 804,
//main: "Clouds",
//description: "overcast clouds",
//icon: "04d"
//}
//],
//pop: 0
//},
//{
//dt: 1686207600,
//temp: 52.86,
//feels_like: 51.28,
//pressure: 1025,
//humidity: 73,
//dew_point: 44.13,
//uvi: 1.2,
//clouds: 21,
//visibility: 10000,
//wind_speed: 4.63,
//wind_deg: 73,
//wind_gust: 6.87,
//weather: [
//{
//id: 801,
//main: "Clouds",
//description: "few clouds",
//icon: "02d"
//}
//],
//pop: 0
//},
//{
//dt: 1686211200,
//temp: 55.15,
//feels_like: 53.51,
//pressure: 1025,
//humidity: 67,
//dew_point: 44.02,
//uvi: 2.2,
//clouds: 33,
//visibility: 10000,
//wind_speed: 5.86,
//wind_deg: 77,
//wind_gust: 8.34,
//weather: [
//{
//id: 802,
//main: "Clouds",
//description: "scattered clouds",
//icon: "03d"
//}
//],
//pop: 0
//},
//{
//dt: 1686214800,
//temp: 57.06,
//feels_like: 55.47,
//pressure: 1024,
//humidity: 64,
//dew_point: 44.53,
//uvi: 3.41,
//clouds: 36,
//visibility: 10000,
//wind_speed: 6.62,
//wind_deg: 72,
//wind_gust: 8.66,
//weather: [
//{
//id: 802,
//main: "Clouds",
//description: "scattered clouds",
//icon: "03d"
//}
//],
//pop: 0
//},
//{
//dt: 1686218400,
//temp: 58.68,
//feels_like: 57.2,
//pressure: 1024,
//humidity: 63,
//dew_point: 45.41,
//uvi: 4.34,
//clouds: 41,
//visibility: 10000,
//wind_speed: 8.12,
//wind_deg: 68,
//wind_gust: 9.71,
//weather: [
//{
//id: 802,
//main: "Clouds",
//description: "scattered clouds",
//icon: "03d"
//}
//],
//pop: 0
//},
//{
//dt: 1686222000,
//temp: 59.95,
//feels_like: 58.6,
//pressure: 1024,
//humidity: 63,
//dew_point: 46.8,
//uvi: 5.23,
//clouds: 45,
//visibility: 10000,
//wind_speed: 8.59,
//wind_deg: 67,
//wind_gust: 9.93,
//weather: [
//{
//id: 802,
//main: "Clouds",
//description: "scattered clouds",
//icon: "03d"
//}
//],
//pop: 0
//}
//],
//daily: [
//{
//dt: 1686052800,
//sunrise: 1686022256,
//sunset: 1686084725,
//moonrise: 1686007560,
//moonset: 1686026940,
//moon_phase: 0.59,
//temp: {
//day: 56.95,
//min: 48,
//max: 57,
//night: 48,
//eve: 54.23,
//morn: 51.31
//},
//feels_like: {
//day: 55.54,
//night: 46.11,
//eve: 52.79,
//morn: 49.95
//},
//pressure: 1027,
//humidity: 68,
//dew_point: 46.51,
//wind_speed: 9.48,
//wind_deg: 60,
//wind_gust: 9.48,
//weather: [
//{
//id: 802,
//main: "Clouds",
//description: "scattered clouds",
//icon: "03d"
//}
//],
//clouds: 42,
//pop: 0,
//uvi: 5.88
//},
//{
//dt: 1686139200,
//sunrise: 1686108610,
//sunset: 1686171192,
//moonrise: 1686096660,
//moonset: 1686118680,
//moon_phase: 0.63,
//temp: {
//day: 59.85,
//min: 44.64,
//max: 60.13,
//night: 49.55,
//eve: 58.19,
//morn: 49.08
//},
//feels_like: {
//day: 58.53,
//night: 47.41,
//eve: 57.09,
//morn: 49.08
//},
//pressure: 1026,
//humidity: 64,
//dew_point: 46.53,
//wind_speed: 11.1,
//wind_deg: 58,
//wind_gust: 13.62,
//weather: [
//{
//id: 800,
//main: "Clear",
//description: "clear sky",
//icon: "01d"
//}
//],
//clouds: 0,
//pop: 0,
//uvi: 6.03
//},
//{
//dt: 1686225600,
//sunrise: 1686194968,
//sunset: 1686257655,
//moonrise: 1686184680,
//moonset: 1686210960,
//moon_phase: 0.66,
//temp: {
//day: 60.96,
//min: 46.09,
//max: 61.36,
//night: 50.52,
//eve: 58.93,
//morn: 50.36
//},
//feels_like: {
//day: 59.77,
//night: 49.41,
//eve: 57.96,
//morn: 48.9
//},
//pressure: 1024,
//humidity: 64,
//dew_point: 47.89,
//wind_speed: 11.41,
//wind_deg: 64,
//wind_gust: 14.76,
//weather: [
//{
//id: 802,
//main: "Clouds",
//description: "scattered clouds",
//icon: "03d"
//}
//],
//clouds: 48,
//pop: 0,
//uvi: 5.65
//},
//{
//dt: 1686312000,
//sunrise: 1686281330,
//sunset: 1686344116,
//moonrise: 1686271980,
//moonset: 1686303300,
//moon_phase: 0.7,
//temp: {
//day: 58.73,
//min: 46.63,
//max: 58.73,
//night: 48.67,
//eve: 54.95,
//morn: 50.56
//},
//feels_like: {
//day: 57.6,
//night: 45.55,
//eve: 53.53,
//morn: 49.26
//},
//pressure: 1021,
//humidity: 70,
//dew_point: 48.27,
//wind_speed: 12.24,
//wind_deg: 66,
//wind_gust: 14,
//weather: [
//{
//id: 804,
//main: "Clouds",
//description: "overcast clouds",
//icon: "04d"
//}
//],
//clouds: 96,
//pop: 0,
//uvi: 6.2
//},
//{
//dt: 1686398400,
//sunrise: 1686367696,
//sunset: 1686430573,
//moonrise: 1686358980,
//moonset: 1686395340,
//moon_phase: 0.75,
//temp: {
//day: 60.33,
//min: 44.8,
//max: 61.54,
//night: 53.69,
//eve: 58.17,
//morn: 49.59
//},
//feels_like: {
//day: 59.31,
//night: 52.99,
//eve: 57.36,
//morn: 47.07
//},
//pressure: 1019,
//humidity: 69,
//dew_point: 49.21,
//wind_speed: 10.4,
//wind_deg: 62,
//wind_gust: 14.03,
//weather: [
//{
//id: 801,
//main: "Clouds",
//description: "few clouds",
//icon: "02d"
//}
//],
//clouds: 24,
//pop: 0,
//uvi: 6.04
//},
//{
//dt: 1686484800,
//sunrise: 1686454065,
//sunset: 1686517026,
//moonrise: 1686445860,
//moonset: 1686487200,
//moon_phase: 0.77,
//temp: {
//day: 56.93,
//min: 51.62,
//max: 60.67,
//night: 57.36,
//eve: 60.67,
//morn: 55.85
//},
//feels_like: {
//day: 56.93,
//night: 57.45,
//eve: 60.91,
//morn: 55.56
//},
//pressure: 1016,
//humidity: 98,
//dew_point: 56.05,
//wind_speed: 5.99,
//wind_deg: 47,
//wind_gust: 10.56,
//weather: [
//{
//id: 500,
//main: "Rain",
//description: "light rain",
//icon: "10d"
//}
//],
//clouds: 100,
//pop: 0.91,
//rain: 6.19,
//uvi: 7
//},
//{
//dt: 1686571200,
//sunrise: 1686540438,
//sunset: 1686603476,
//moonrise: 1686532680,
//moonset: 1686578880,
//moon_phase: 0.81,
//temp: {
//day: 64.24,
//min: 56.37,
//max: 67.17,
//night: 59.94,
//eve: 64.51,
//morn: 58.64
//},
//feels_like: {
//day: 64.13,
//night: 60.13,
//eve: 64.89,
//morn: 58.71
//},
//pressure: 1013,
//humidity: 80,
//dew_point: 57.31,
//wind_speed: 4.16,
//wind_deg: 244,
//wind_gust: 7.52,
//weather: [
//{
//id: 500,
//main: "Rain",
//description: "light rain",
//icon: "10d"
//}
//],
//clouds: 96,
//pop: 0.91,
//rain: 4.38,
//uvi: 7
//},
//{
//dt: 1686657600,
//sunrise: 1686626815,
//sunset: 1686689922,
//moonrise: 1686619500,
//moonset: 1686670440,
//moon_phase: 0.84,
//temp: {
//day: 61.54,
//min: 56.62,
//max: 61.56,
//night: 56.62,
//eve: 59.22,
//morn: 58.68
//},
//feels_like: {
//day: 61.52,
//night: 56.55,
//eve: 59.22,
//morn: 58.66
//},
//pressure: 1013,
//humidity: 88,
//dew_point: 57.29,
//wind_speed: 5.3,
//wind_deg: 60,
//wind_gust: 8.16,
//weather: [
//{
//id: 500,
//main: "Rain",
//description: "light rain",
//icon: "10d"
//}
//],
//clouds: 99,
//pop: 0.67,
//rain: 1.64,
//uvi: 7
//}
//]
//}

// **** MY API DATA ****

// {
// - coord: {
//    lon: -0.1257,
//    lat: 51.5085
// },
// - weather: [
// {
//    id: 803,
//    main: "Clouds",
//    description: "broken clouds",
//    icon: "04d"
// }
// ],
// base: "stations",
// - main: {
//    temp: 56.44,
//    feels_like: 55.17,
//    temp_min: 53.35,
//    temp_max: 60.13,
//    pressure: 1023,
//    humidity: 72
// },
// visibility: 10000,
// - wind: {
//    speed: 9.22,
//    deg: 50
// },
// - clouds: {
//    all: 75
// },
// dt: 1686043724,
// - sys: {
//    type: 2,
//    id: 268730,
//    country: "GB",
//    sunrise: 1686023160,
//    sunset: 1686082351
// },
// timezone: 3600,
// id: 2643743,
// name: "London",
// cod: 200
// }

//struct APIResponse: Codable {
//    let coord: CoordModel
//    let main: MainTemp
//    let weather: [WeatherType]
//}
//
//struct CoordModel: Codable {
//    let lon: Float
//    let lat: Float
//}
//
//struct MainTemp: Codable {
//    let temp: Double
//    let feels_like: Double
//    let temp_min: Double
//    let temp_max: Double
//    let pressure: Int
//    let humidity: Int
//}
//
//struct WeatherType: Codable {
//    let id: Int
//    let main: String
//    let description: String
//    let icon: String
//}
