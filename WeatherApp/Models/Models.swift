//
//  Models.swift
//  WeatherApp
//
//  Created by Aidan Walker on 30/05/2023.
//
import CoreLocation
import Foundation
import SwiftUI

class WeatherViewModel: ObservableObject {
// @Published - retriggers the Views
    @Published var headerViewModel = HeaderViewModel()
    @Published var hourlyData: [HourData] = []
    @Published var dailyData: [DayData] = []
// @Published var hourlyModel = HourlyModel()
    
//    @Published var dailyModel = DailyModel()
//    var headerViewModel = HeaderViewModel()
//    var hourlyModel = HourlyModel()
//    var dailyModel = DailyModel()
    
    init() {
        fetchData()
    }
    
    func fetchData() {
        // get data and location info
        // let urlString = "https://api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=4fcfa281f0d08e049e6b6ff7ee278e41&units=imperial" //myapi
        let urlString = "https://api.openweathermap.org/data/2.5/onecall?lat=55.953251&lon=-3.188267&exclude=minutely&units=imperial&appid=c0fe764b892253282a9c1701b3bf72b5"
        
        //tutorial api
        guard let url = URL(string: urlString) else {
            return
        }
        // with: url - endpoint, _ - gives back a response which we can ignore
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data, error == nil else {
                return
            }
            // convert data to api response
//            let result: APIResponse?
            do {
                let result = try JSONDecoder().decode(APIResponse.self, from: data)

                DispatchQueue.main.async {
                    // Hourly
                    self.headerViewModel.currentTemp = "\(Int(result.current.temp))°F"
                    self.headerViewModel.currentConditions = result.current.weather.first?.main ?? "-"
                    self.headerViewModel.iconURLString = String.iconUrlString(for: result.current.weather.first?.icon ?? "")
                    // Hourly
                    self.hourlyData = result.hourly.compactMap({
                        let data = HourData()
                        data.temp = "\(Int($0.temp))°"
                        data.hour = String.hour(from: $0.dt)
                        data.imageURL = String.iconUrlString(for: $0.weather.first?.icon ?? "")
                        return data
                    })

                    // Daily
                    self.dailyData = result.daily.compactMap({
                        let data = DayData()
                        data.day = String.day(from: $0.dt)
                        data.high = "\($0.temp.max)°F"
                        data.low = "\($0.temp.min)°F"
                        return data
                    })
                }

                return
            }
            catch {
                print(error)
            }
        }
        task.resume()
    }
}

// MARK: - Header

class HeaderViewModel: ObservableObject {
    var location: String = "New York City, NY"
    var currentTemp: String = "75°"
    var currentConditions: String = "Clear"
    var iconURLString = "https://www.apple.com"
}

// MARK: - Hourly

// Theres no point holding a model with one observable data in it

// class HourlyModel: ObservableObject {
//    var data: [HourData] = []
// }

class HourData: ObservableObject, Identifiable {
    var id = UUID()
    var temp = "55ª"
    var hour = "1PM"
    var imageURL = "https://www.apple.com"
}

// MARK: - Daily

// class DailyModel: ObservableObject {
//    var data: [DayData] = []
// }

class DayData: ObservableObject, Identifiable {
    var id = UUID()
    var day = "Monday"
    var high = "77°F"
    var low = "47°F"
}

