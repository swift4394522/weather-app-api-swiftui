//
//  ContentView.swift
//  WeatherApp
//
//  Created by Aidan Walker on 30/05/2023.
//

import SwiftUI

struct ContentView: View {
    // by using an environment model, we can call the api from one place
    // were able to access the view model data from all the Views and prevents redundandtly passing it down from the ContentView
    @EnvironmentObject var viewModel: WeatherViewModel
    
    var body: some View {
        // /topLeading - topLeft, .trailing - bottom right
        ZStack {
            LinearGradient(gradient: Gradient(colors: [.blue, Color(.link), .pink]), startPoint: .topLeading, endPoint: .trailing)
                .edgesIgnoringSafeArea(.all)
            ScrollView {
                VStack {
                    HeaderView()
                    HourlyView()
                    DailyView()
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
