//
//  HourlyView.swift
//  WeatherApp
//
//  Created by Aidan Walker on 30/05/2023.
//

import SwiftUI

struct HourlyView: View {
    // var viewModel = WeatherViewModel()
    @EnvironmentObject var viewModel: WeatherViewModel
    
    var body: some View {
        HStack {
            ScrollView(.horizontal) {
                HStack {
                   // viewModel.hourlyData
                    ForEach(viewModel.hourlyData) { model in
                        HourView(model: model)
                    }
                }
            }
        }
    }
}

struct HourView: View {
    var model: HourData
    
    var body: some View {
        VStack {
            // image, temp, hour
            Image(systemName: "sun.max.fill")
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 35, height: 35, alignment: .center)
            Text(model.temp)
                .bold()
                .foregroundColor(.white)
            Text(model.hour)
                .foregroundColor(.white)
        }
        .padding()
    }
}

struct HourlyView_Previews: PreviewProvider {
    static var previews: some View {
        HourlyView()
    }
}
