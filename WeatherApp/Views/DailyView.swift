//
//  DailyView.swift
//  WeatherApp
//
//  Created by Aidan Walker on 30/05/2023.
//

import SwiftUI

struct DailyView: View {
//    var viewModel = WeatherViewModel()
    @EnvironmentObject var viewModel: WeatherViewModel
    
    var body: some View {
        VStack {
            // viewModel.dailyData
            ForEach(viewModel.dailyData) { model in
                DayRowView(model: model)
                    .padding()
            }
        }
    }
}

struct DayRowView: View {
    var model: DayData
    
    var body: some View {
        HStack {
            Text(model.day)
                .bold()
                .font(.system(size: 32))
                .foregroundColor(.white)
            
            Spacer()
            
            VStack {
                Text("H: \(model.high)")
                    .foregroundColor(.white)
                Text("L: \(model.low)")
                    .foregroundColor(.white)
            }
        }
    }
}

struct DailyView_Previews: PreviewProvider {
    static var previews: some View {
        DailyView()
    }
}
