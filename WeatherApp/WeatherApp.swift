//
//  WeatherApp.swift
//  WeatherApp
//
//  Created by Aidan Walker on 30/05/2023.
//

import SwiftUI

@main
struct WeatherApp: App {
    var body: some Scene {
        WindowGroup {
            let viewModel = WeatherViewModel()
            ContentView().environmentObject(viewModel)
//            ContentView()
        }
    }
}
