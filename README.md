<h1 align="center">
<br>
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/swift/swift-original.svg" width="80px" />       
<br>
<br>
Weather UI - SwiftUI
</h1>

<h4 align="center">Weather UI with real data from openweatherapi</h4>
<p align="center">. Content View as entry point which contains tab views for managing 5 distinct tabs</p>
<p align="center">. Home feed comprises of stories view and post feed view</p>
<p align="center">. Explore view contains 2 column grids with search, tap on image via navigation link to show post</p>
<p align="center">. Camera view hooks into uikit as its not included in swiftui. Hooks into photo library and camera</p>

<div align="center">
   <img align="center" src="./WeatherApp/weather.gif" width="230px">
</div>
